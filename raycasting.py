import pygame as pg
import math
from settings import *


# Creating the raycasting class in the attributes of which we take an instance of our game and define
# the raycast method which will be called from the update method:
class RayCasting:
    def __init__(self, game):
        self.game = game
        # Defining the attributes for the result of raycasting, the objects to be drawn and we define
        # a short name for the wall textures:
        self.ray_casting_result = []
        self.objects_to_render = []
        self.textures = self.game.object_renderer.wall_textures

    # A method to actually get objects to draw:
    def get_objects_to_render(self):
        # the task here is to get the parameters calculated as a result of ray casting; and based on
        # them, for each ray select a subsurface in the form of a rectangle from the initial texture
        self.objects_to_render = []
        for ray, values in enumerate(self.ray_casting_result):
            depth, proj_height, texture, offset = values

            # Eliminating the bug of FPS dropping when getting close to textured surfaces (walls particularly):
            # Technically put: we need to consider the case when the projection height of the wall exceeds
            # the height resolution of the screen:
            if proj_height < HEIGHT:
                wall_column = self.textures[texture].subsurface(
                    offset * (TEXTURE_SIZE - SCALE), 0, SCALE, TEXTURE_SIZE
                )
                wall_column = pg.transform.scale(wall_column, (SCALE, proj_height))
                wall_pos = (ray * SCALE, HALF_HEIGHT - proj_height // 2)
            else:
                texture_height = TEXTURE_SIZE * HEIGHT / proj_height
                wall_column = self.textures[texture].subsurface(
                    offset * (TEXTURE_SIZE - SCALE), HALF_TEXTURE_SIZE - texture_height //2,
                    SCALE, texture_height
                )
                wall_column = pg.transform.scale(wall_column, (SCALE, HEIGHT))
                wall_pos = (ray * SCALE, 0)


            self.objects_to_render.append((depth, wall_column, wall_pos))


    # In the raycast method we calculate the angle for the first ray; we need to substract half the value of the field
    # of view from the player's angle and add a small value in order to avoid the division by zero error in further
    # calculations
    def ray_cast(self):
        # Clearing the list before starting the process:
        self.ray_casting_result = []
        # A thing to avoid troubles:
        texture_vert, texture_hor = 1, 1
        # We also need the coordinates of the player on the map, AND the coordinates of his tile (x-map y-map)
        ox, oy = self.game.player.pos
        x_map, y_map = self.game.player.map_pos


        ray_angle = self.game.player.angle - HALF_FOV + 0.0001
        # Now, let's write a loop on the number of rays, with the help of which we calculate the angles for
        # each array
        for ray in range(NUM_RAYS):
            # In this loop we also calculate the values of the sin and cos of the current ray and proceed
            # to the case of the intersection of the ray with verticals
            sin_a = math.sin(ray_angle)
            cos_a = math.cos(ray_angle)

            # HORIZONTALS
            # And here, by the value of the sin sign we will determine the value of the variable dy:
            y_hor, dy = (y_map +1, 1) if sin_a > 0 else (y_map - 1e-6, -1)

            depth_hor = (y_hor - oy) / sin_a
            x_hor = ox + depth_hor * cos_a

            #Casting a ray and finding the intersections with verticals:
            delta_depth = dy / sin_a
            dx = delta_depth * cos_a

            # Now we will cast the ray in a cycle by the number of steps equal to the value of the maximum depth:
            for i in range(MAX_DEPTH):
                tile_hor = int(x_hor), int(y_hor)
                # if hit the wall - stop
                if tile_hor in self.game.map.world_map:
                    # determining the exact number of the textures of the walls, into which the rays collided (horizontal calc):
                    texture_hor = self.game.map.world_map[tile_hor]
                    break
                x_hor += dx
                y_hor += dy
                depth_hor += delta_depth

            # VERTICALS

            # And here, by the value of the cos sign we will determine the value of the variable dx:
            x_vert, dx = (x_map +1, 1) if cos_a > 0 else (x_map - 1e-6, -1)

            depth_vert = (x_vert - ox) / cos_a
            y_vert = oy + depth_vert * sin_a

            #Casting a ray and finding the intersections with verticals:
            delta_depth = dx / cos_a
            dy = delta_depth * sin_a

            # Now we will cast the ray in a cycle by the number of steps equal to the value of the maximum depth:
            for i in range(MAX_DEPTH):
                tile_vert = int(x_vert), int(y_vert)
                # if hit the wall - stop
                if tile_vert in self.game.map.world_map:
                    # determining the exact number of the textures of the walls, into which the rays collided (vertical calc):
                    texture_vert = self.game.map.world_map[tile_vert]
                    break
                x_vert += dx
                y_vert += dy
                depth_vert += delta_depth

            # DEPTH, texture offset
            if depth_vert < depth_hor:
                depth, texture = depth_vert, texture_vert
                # Now, based on the calculated depths of the rays we find the actual texture number,
                # while calculating the correct offset in the way that was defined above
                y_vert %= 1
                offset = y_vert if cos_a > 0 else (1 - y_vert)
            else:
                depth, texture = depth_hor, texture_hor
                x_hor %= 1
                offset = (1 - x_hor) if sin_a > 0 else x_hor

            # remove fishbowl effect (the walls a getting "curvy" when a player approach, that i a result
            # of simultaneous use of cartesian coordinate system with the polar one):
            depth *= math.cos(self.game.player.angle - ray_angle)

            # drawing for debugging
            # pg.draw.line(self.game.screen, 'yellow', (100 * ox, 100 * oy),
            #             (100 * ox + 100 * depth * cos_a, 100 * oy + 100 * depth * sin_a), 2)

            # projection
            proj_height = SCREEN_DIST / (depth + 0.0001)

            # In order to add depth to the lightning in the game we calculate the color in some power
            # dependence on the value of the depth of the ray
            # color = [255 / (1 + depth ** 5 * 0.00002)] * 3

            #ray casting result
            self.ray_casting_result.append((depth, proj_height, texture, offset))

            ray_angle += DELTA_ANGLE

    def update(self):
        self.ray_cast()
        self.get_objects_to_render()