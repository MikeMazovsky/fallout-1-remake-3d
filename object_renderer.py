import pygame as pg
from settings import *


# Here we create an object render class that will render all objects in the game. We will take the
# game instance in the rendering screen as attributes of this class
class ObjectRenderer:
    def __init__(self, game):
        self.game = game
        self.screen = game.screen
        # Calling the texture loading method:
        self.wall_textures = self.load_wall_textures()
        self.sky_image = self.get_texture('data/textures/Nature/desert_skies_004.jpg', (WIDTH, HALF_HEIGHT))
        self.sky_offset = 0
        self.blood_screen = self.get_texture('data/textures/blood_screen.png', RES)
        self.digit_size = 90
        self.digit_images = [self.get_texture(f'data/textures/digits/{i}.png', [self.digit_size] * 2)
                             for i in range(11)]
        self.digits = dict(zip(map(str, range(11)), self.digit_images))
        self.game_over_image = self.get_texture('data/textures/game_over.png', RES)
        self.win_image = self.get_texture('data/textures/win.png', RES)

    #We will the work of the method below through the main draw method:
    def draw(self):
        self.draw_background()
        self.render_game_objects()
        self.draw_player_health()

    def win(self):
        self.screen.blit(self.win_image, (0, 0))

    def game_over(self):
        self.screen.blit(self.game_over_image, (0, 0))

    def draw_player_health(self):
        health = str(self.game.player.health)
        for i, char in enumerate(health):
            self.screen.blit(self.digits[char], (i * self.digit_size, 0))
        self.screen.blit(self.digits['10'], ((i + 1) * self.digit_size, 0))

    def player_damage(self):
        self.screen.blit(self.blood_screen, (0, 0))

    # This method will calculate the offset, depending on the relative mouse movement value for the player instance:
    def draw_background(self):
        self.sky_offset = (self.sky_offset + 4.5 * self.game.player.rel) % WIDTH
        # Using the blit method we will place two sky textures, taking into account the calculated offset value:
        self.screen.blit(self.sky_image, (-self.sky_offset, 0))
        self.screen.blit(self.sky_image, (-self.sky_offset + WIDTH, 0))
        # Floor as a rectangle of the assigned color (temporary solution)
        pg.draw.rect(self.screen, FLOOR_COLOR, (0, HALF_HEIGHT, WIDTH, HEIGHT))

    # Here, using a separate method through an instance of the game class, we will get access to the list
    # of objects for rendering;  iterate over this list and draw the resulting texture columns on the rendering screen:
    def render_game_objects(self):
        # Here we are setting the rendering priority in order to fix the "Almighty-always visible items" bug
        list_objects = sorted(self.game.raycasting.objects_to_render, key=lambda t: t[0], reverse = True)
        for depth, image, pos in list_objects:
            self.screen.blit(image, pos)

    # For convenience, we wil create a static method, for which the path to the texture and its resolution are
    # specified. This method loads the texture from the specified path, and returns the scaled image.
    @staticmethod
    def get_texture(path, res=(TEXTURE_SIZE, TEXTURE_SIZE)):
        texture = pg.image.load(path).convert_alpha()
        return pg.transform.scale(texture, res)

    # Writing a method for loading textures. It will return a dictionary, in which the texture number is
    # the key, and the texture itself is the value.
    def load_wall_textures(self):
        return {
            1: self.get_texture('data/textures/Buildings/Brickwall_001.png'),
            2: self.get_texture('data/textures/Buildings/Brickwall_window_001.png'),
            3: self.get_texture('data/textures/Buildings/Brickwall_door_001.png'),
            4: self.get_texture('data/textures/Buildings/Concrete_building_door_001.png'),
            5: self.get_texture('data/textures/Buildings/Concrete_building_wall_001.png'),
            6: self.get_texture('data/textures/Buildings/Concrete_building_wall_002.png'),
            7: self.get_texture('data/textures/Buildings/old_concrete_wall_001.png'),
        }