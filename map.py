# DISCLAIMER: THIS A TESTING MAP FOR ME AND ANY OTHER POSSIBLE DEVELOPERS.
# THE ONLY PURPOSE HERE IS TO SHOW THE MECHANISM OF MAP BUILDING, WHICH IS
# TEMPORARY PRESENTED IN THIS STATE

import pygame as pg


# We will create a two-dimensional array here, where integer digital values
# will represent the "walls", string characters will (probably) represent the objects,
# and False values will represent free space. For better representation we marked them as "_"  symbol
# This concept is to be replaced by importing text files of different maps. Also, the exact number here
# represents the number of texture that is loaded and later displayed in that point on map in the game.

_ = False
mini_map = [
    [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, 2, 1, 1, _, _, _, _, 2, 1, 1, _, _, 5],
    [5, _, _, 3, _, 2, _, _, _, _, 3, _, 2, _, _, 5],
    [5, _, _, 2, 1, 1, _, _, _, _, 2, 1, 1, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 5],
    [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
]

# Creating a map class: here we get an instance of the game class as input
# to the constructor; at the same time, the minimap and the world map become
# attributes

class Map:
    def __init__(self, game):
        self.game = game
        self.mini_map = mini_map
        self.world_map = {}
        self.rows = len(self.mini_map)
        self.cols = len(self.mini_map[0])
        self.get_map()

# The world map above will be obtained through a separate method, in which
# iterate over our array and write the coordinates of elements with only
# numeric values to the dictionary

    def get_map(self):
        for j, row in enumerate(self.mini_map):
            for i, value in enumerate(row):
                if value:
                    self.world_map[(i, j)] = value

# Now, in order to display our map on the screen we will write a test method
# draw. Iterating over the world, we will draw each element of the map as an
# unfilled square

    def draw(self):
        [pg.draw.rect(self.game.screen, 'darkgray', (pos[0] * 100, pos[1] * 100, 100, 100), 2)
         for pos in self.world_map]
