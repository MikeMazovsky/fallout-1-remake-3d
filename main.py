import sys
import pygame as pg
from settings import *
from map import *
from player import *
from raycasting import *
from object_renderer import *
from sprites import *
from object_handler import *
from weapon import *
from sound import *
from pathfinding import *

# Creating a game class in the constructor of which we initialize the pygame modules;
# Create a screen for rendering the set resolution in an instance of the clock class for the frame rate;
class Game:
    def __init__(self):
        pg.init()
        pg.mouse.set_visible(False)
        self.screen = pg.display.set_mode(RES)
        self.clock = pg.time.Clock()
        # Delta Time is the amount of time that has passed since the last frame, see the player.py for more info
        self.delta_time = 1
        self.global_trigger = False
        self.global_event = pg.USEREVENT + 0
        pg.time.set_timer(self.global_event, 40)
        self.new_game()

# We will call all needed methods from here
    def new_game(self):
        self.map = Map(self)
        self.player = Player(self)
        self.object_renderer = ObjectRenderer(self)
        self.raycasting = RayCasting(self)
        self.object_handler = ObjectHandler(self)
        self.weapon = Weapon(self)
        self.sound = Sound(self)
        self.pathfinding = PathFinding(self)
        pg.mixer.music.play(-1)

    # Using "update" method, the program will update the screen and display information about the current number of frames per
# second in the window caption, as well as perform other modules implementations. If anything is unclear so far - just take a look at related files content (aka player.py, map.py etc.)
    def update(self):
        self.player.update()
        self.raycasting.update()
        self.object_handler.update()
        self.weapon.update()
        pg.display.flip()
        self.delta_time = self.clock.tick(FPS)
        pg.display.set_caption(f'{self.clock.get_fps() :.1f}')

    # Creating a "draw" method, so at each iteration it will paint the screen in black
    def draw(self):
        #self.screen.fill('black')
        self.object_renderer.draw()
        self.weapon.draw()
        #self.map.draw()
        #self.player.draw()

# This method purpose is checking the events for closing the working window
# and pressing the "Escape" button, so if any of these events happen - the application
# will be closed correctly. It will also be called from the main loop (see the next method, "run")
    def check_events(self):
        self.global_trigger = False
        for event in pg.event.get():
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()
            elif event.type == self.global_event:
                self.global_trigger = True
            self.player.single_fire_event(event)

# We are also going to need this "run" method, which will contain the main loop of the game.
# From this loop will call the update and draw methods
    def run(self):
        while True:
            self.check_events()
            self.update()
            self.draw()

# Here I will create a testing instance of the game and call the previously defined "run" method, feel free to experiment with it too
if __name__ == '__main__':
        game = Game()
        game.run()