import math
import pygame as pg
from settings import *
from map import *


# Creating a player class in the attributes of which we will take an instance of the game;
# x and y coordinates of the player; and finally, the angle of player's direction:
class Player:
    def __init__(self, game):
        self.game = game
        self.x, self.y = PLAYER_POS
        self.angle = PLAYER_ANGLE
        # Adding the Gameplay functionality part:
        self.shot = False
        self.health = PLAYER_MAX_HEALTH
        self.rel = 0
        self.health_recovery_delay = 700
        self.time_prev = pg.time.get_ticks()

    def recover_health(self):
        if self.check_health_recovery_delay() and self.health < PLAYER_MAX_HEALTH:
            self.health += 1

    def check_health_recovery_delay(self):
        time_now = pg.time.get_ticks()
        if time_now - self.time_prev > self.health_recovery_delay:
            self.time_prev = time_now
            return True

    def check_game_over(self):
        if self.health < 1:
            self.game.object_renderer.game_over()
            pg.display.flip()
            pg.time.delay(1500)
            self.game.new_game()

    def get_damage(self, damage):
        self.health -= damage
        self.game.object_renderer.player_damage()
        self.game.sound.player_pain.play()
        self.check_game_over()

    def single_fire_event(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            if event.button == 1 and not self.shot and not self.game.weapon.reloading:
                self.game.sound.shotgun.play()
                self.shot = True
                self.game.weapon.reloading = True

    # Creating the movement method based on finding trigonometrical functions for dx and dy:
    # IMPORTANT NOTE: since we want the player's movement speed to be independent of the frame rate, we need to
    # get the Delta time value for each frame. Delta Time is the amount of time that has passed since the last frame.
    # That is exactly why we created delta value in the main.py file, inside the movement function.
    def movement(self):
        sin_a = math.sin(self.angle)
        cos_a = math.cos(self.angle)
        dx, dy = 0, 0

        # Here we are going to adjust the speed to the Delta Time
        speed = PLAYER_SPEED * self.game.delta_time

        # We also pre-calculate the product of the obtained speed. Firstly, let's calculate values for the sin and cos:
        speed_sin = speed * sin_a
        speed_cos = speed * cos_a

        # Now we are going to obtain information about the keys pressed by calculating the increments dx and dy
        keys = pg.key.get_pressed()
        if keys[pg.K_w]:
            dx += speed_cos
            dy += speed_sin
        if keys[pg.K_a]:
            dx += speed_sin
            dy += -speed_cos
        if keys[pg.K_s]:
            dx += -speed_cos
            dy += -speed_sin
        if keys[pg.K_d]:
            dx += -speed_sin
            dy += speed_cos

# This part was replaced by the check_wall_collision method
        # Applying received increments to the corresponding coordinates of the player:
        #self.x += dx
        #self.y += dy
        self.check_wall_collision(dx, dy)
        # Implementing control of the player's direction (for now, let's keep it a rudimentary left-right keys,
        # while angle value would remain within 2p):
        #if keys[pg.K_LEFT]:
        #    self.angle -= PLAYER_ROT_SPEED * self.game.delta_time
        #if keys[pg.K_RIGHT]:
        #    self.angle += PLAYER_ROT_SPEED * self.game.delta_time
        #Note: tau is 2*Pi
        self.angle %= math.tau

# Creating a method that checks if the checked coordinates hit the wall:
    def check_wall(self, x, y):
        return(x, y) not in self.game.map.world_map

# Using the method created above in the "collision check" method: using the computed dx and dy increments
# in turn we check the new coordinates, and only allow movements if there is no wall:
    def check_wall_collision(self, dx, dy):
        # Since increments dx and dy depend on the delta time, but the size of the player should not depend on this
        # delta time value (because we will find the product of these values):
        scale = PLAYER_SIZE_SCALE / self.game.delta_time
        if self.check_wall(int(self.x + dx * scale), int(self.y)):
            self.x += dx
        if self.check_wall(int(self.x), int(self.y + dy * scale)):
            self.y += dy

# Writing a test method drawing a player on a plane. We draw his direction of movement as a line, and
# the player himself will be in the form of a circle:
    def draw(self):
        pg.draw.line(self.game.screen, 'yellow', (self.x * 100, self.y * 100),
                     (self.x * 100 + WIDTH * math.cos(self.angle),
                     self.y * 100 + WIDTH * math.sin(self.angle)), 2)
        pg.draw.circle(self.game.screen, 'green', (self.x * 100, self.y * 100), 15)

# Mouse basic method:
    def mouse_control(self):
        # Getting the mouse coordinates
        mx, my = pg.mouse.get_pos()
        # Check if the x-coordinate is within the bounds; if not, the setting the cursor to the middle of the screen
        if mx < MOUSE_BORDER_LEFT or mx > MOUSE_BORDER_RIGHT:
            pg.mouse.set_pos([HALF_WIDTH, HALF_HEIGHT])
        # Getting the value of the revative mouse movement since the previous frame and clamped this value; as
        # a result, changing the angle of the player's direction to the value "rel", taking into account the
        # values of sensitivity and delta time:
        self.rel = pg.mouse.get_rel()[0]
        self.rel = max(-MOUSE_MAX_REL, min(MOUSE_MAX_REL, self.rel))
        self.angle += self.rel * MOUSE_SENSIVITY * self.game.delta_time

# Using "update" method for calling the "movement" method:
    def update(self):
        self.movement()
        self.mouse_control()
        self.recover_health()

# The next is made for convenience. We shall create two properties:
# 1) First to return player's coordinates
    @property
    def pos(self):
        return self.x, self.y

# 2) Second is an integer value, to know which tile of the map are we currently on
    @property
    def map_pos(self):
        return int(self.x), int(self.y)
