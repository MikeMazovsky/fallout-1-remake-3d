import math

# INITIAL GAME SETTINGS:

RES = WIDTH, HEIGHT = 1920, 1080
FPS = 0
# Calculating half the resolution and half the height (we need to do it to simplify further scripting):
HALF_WIDTH = WIDTH // 2
HALF_HEIGHT = HEIGHT //2

# INITIAL PLAYER SETTINGS

# Defining the position of the player on the map:
PLAYER_POS = 1.5, 5 # for default mini_Map
# The angle of player's direction:
PLAYER_ANGLE = 0
# The speed of player's movement:
PLAYER_SPEED = 0.004
# The speed of player's rotation:
PLAYER_ROT_SPEED = 0.003
# Defining the player's character size; by previous settings, player was considered to be a dot.
PLAYER_SIZE_SCALE = 60

PLAYER_MAX_HEALTH = 100


# MOUSE PARAMETERS

MOUSE_SENSIVITY = 0.0003
# Maximum relative movement
MOUSE_MAX_REL = 40
MOUSE_BORDER_LEFT = 100
MOUSE_BORDER_RIGHT = WIDTH - MOUSE_BORDER_LEFT

# FLOOR AND SKIES PARAMETERS; TEMPORARILY ASSIGNED
FLOOR_COLOR = (30, 30, 30)

# RAYCASTING AND RENDERING

# Player's available field of view. For the whole game it is enough to set the half of the resolution witdh
# value; also, based on the number of rays we will define the angle between the rays (the DELTA_ANGLE) and
# define the maximum depth (you can roughly call it a draw-distance):
FOV = math.pi / 3
HALF_FOV = FOV / 2
NUM_RAYS = WIDTH // 2
HALF_NUM_RAYS = NUM_RAYS // 2
DELTA_ANGLE = FOV / NUM_RAYS
MAX_DEPTH = 20

# Calculating the correct distance for the screen location
SCREEN_DIST = HALF_WIDTH / math.tan(HALF_FOV)

# Determining the scaling factor: since the number of rays we have is less than the screen resolution in
# width, this is done specifically to maintain better performance:
SCALE = WIDTH // NUM_RAYS

# Defining the size for the textures in the game and calculating the half of its value;
TEXTURE_SIZE = 256
HALF_TEXTURE_SIZE = TEXTURE_SIZE // 2

